﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Respositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasUnitarias
{
    class LibroControllerTest
    {
        [Test]
        public void DetallesLibro() {
            var repositoryL =new Mock<ILibroRepository>();
            repositoryL.Setup(o => o.LibroDetalles(It.IsAny<int>())).Returns(new Libro());
            var repositoryU = new Mock<IUsuarioRepository>();
            var controller = new LibroController(repositoryL.Object,repositoryU.Object);
            var result = controller.Details(It.IsAny<int>());
            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void AñadirComentarioCorrectamente()
        {
            var repository = new Mock<ILibroRepository>();
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o=>o.GetLoggedUser()).Returns(new Usuario());
            repository.Setup(o => o.AddComentario(It.IsAny<Comentario>(), It.IsAny<Usuario>()));
            var controller = new LibroController(repository.Object,repositoryU.Object);
            var result = controller.AddComentario(new Comentario {LibroId = 1 });
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }

    }
}
