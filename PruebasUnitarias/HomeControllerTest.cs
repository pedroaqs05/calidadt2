﻿using CalidadT2.Controllers;
using CalidadT2.Respositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasUnitarias
{
    class HomeControllerTest
    {
        [Test]
        public void Visualizarlibros()
        {
            var mockLibros = new Mock<ILibroRepository>();
            var controller = new HomeController(mockLibros.Object);
            var result = controller.Index();
            Assert.IsInstanceOf<ViewResult>(result);
        }

    }
}
