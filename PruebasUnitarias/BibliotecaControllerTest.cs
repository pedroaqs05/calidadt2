﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Respositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasUnitarias
{
    class BibliotecaControllerTest
    {
        [Test]
        public void VerBibliotecadCorrectamente(){
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repositoryB = new Mock<IBibiotecaRepository>();
            var controller = new BibliotecaController(repositoryU.Object, repositoryB.Object);
            var result = controller.Index();
            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void AñadirLibroCorrectamente()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repositoryB = new Mock<IBibiotecaRepository>();
            var controller = new BibliotecaController(repositoryU.Object, repositoryB.Object);
            var result = controller.Add(It.IsAny<int>());
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void MarcarLibroComoLeyendoCorrectamente()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repositoryB = new Mock<IBibiotecaRepository>();
            var controller = new BibliotecaController(repositoryU.Object, repositoryB.Object);
            var result = controller.MarcarComoLeyendo(It.IsAny<int>());
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void MarcarLibroComoTerminadoCorrectamente()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.GetLoggedUser()).Returns(new Usuario());
            var repositoryB = new Mock<IBibiotecaRepository>();
            var controller = new BibliotecaController(repositoryU.Object, repositoryB.Object);
            var result = controller.MarcarComoTerminado(It.IsAny<int>());
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }

    }
}
