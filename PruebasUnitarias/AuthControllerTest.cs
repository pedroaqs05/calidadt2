﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Respositories;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace PruebasUnitarias
{
    class AuthControllerTest
    {
        [Test]
        public void LoginCorrecto()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            mockRepository.Setup(o => o.FindUserLogin("admin", It.IsAny<String>()))
                .Returns(new Usuario { Username = "admin", Password = "admin" });
            var cookie = new Mock<ICookieAuthService>();

            var controller = new AuthController(cookie.Object,mockRepository.Object);
            
            var result = controller.Login("admin", "admin");
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
        [Test]
        public void LoginIncorrecto()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            var cookie = new Mock<ICookieAuthService>();
            var controller = new AuthController(cookie.Object, mockRepository.Object);

            var result = controller.Login("admin","admin");
            Assert.IsInstanceOf<ViewResult>(result);
        }
        [Test]
        public void LogoutCorrecto()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            var cookie = new Mock<ICookieAuthService>();
            var controller = new AuthController(cookie.Object, mockRepository.Object);
            var result = controller.Logout();
            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
    }
}
