﻿using CalidadT2.Constantes;
using CalidadT2.Models;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Respositories
{
    public interface IBibiotecaRepository
    {
        public List<Biblioteca> VerBiblioteca(Usuario user);
        public void AddLibro(int libro, Usuario user);
        public void SetTempData(ITempDataDictionary _tempData);
        public void MarcarLeyendo(int libroId, Usuario user);
        public void MarcarTerminado(int libroId, Usuario user);
    }
    public class BibiotecaRepository : IBibiotecaRepository
    {
        private readonly AppBibliotecaContext app;
        private ITempDataDictionary tempData;
        public BibiotecaRepository(AppBibliotecaContext _app)
        {
            this.app = _app;
        }
        public void SetTempData(ITempDataDictionary _tempData)
        {
            this.tempData = _tempData;
        }
        public List<Biblioteca> VerBiblioteca(Usuario user)
        {
            return app.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == user.Id)
                .ToList();
        }
        public void AddLibro(int libro, Usuario user)
        {
            if (app.Bibliotecas.Where(o=>o.LibroId== libro).Count()>0)
            {
                tempData["ErrorMessage"] = "Ya tiene el libro en su biblioteca se le habilito la lectura nuevamente";
                var libroDb = app.Bibliotecas
                 .Where(o => o.LibroId == libro && o.UsuarioId == user.Id)
                 .FirstOrDefault();

                libroDb.Estado = ESTADO.POR_LEER;
                app.SaveChanges();
            }
            else
            {
                var biblioteca = new Biblioteca
                {
                    LibroId = libro,
                    UsuarioId = user.Id,
                    Estado = ESTADO.POR_LEER
                };
                app.Bibliotecas.Add(biblioteca);
                app.SaveChanges();
                tempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";
            }
        }

        public void MarcarLeyendo(int libroId, Usuario user)
        {
            var libro = app.Bibliotecas
                 .Where(o => o.LibroId == libroId && o.UsuarioId == user.Id)
                 .FirstOrDefault();

            libro.Estado = ESTADO.LEYENDO;
            app.SaveChanges();

            tempData["SuccessMessage"] = "Se marco como leyendo el libro";
        }

        public void MarcarTerminado(int libroId, Usuario user)
        {
            var libro = app.Bibliotecas
                 .Where(o => o.LibroId == libroId && o.UsuarioId == user.Id)
                 .FirstOrDefault();

            libro.Estado = ESTADO.TERMINADO;
            app.SaveChanges();

            tempData["SuccessMessage"] = "Se marco como terminado el libro";
        }

    }
}
