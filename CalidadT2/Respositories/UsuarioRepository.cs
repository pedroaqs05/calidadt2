﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Respositories
{
    public interface IUsuarioRepository
    {
        public Usuario FindUserLogin(string username, string password);
        public Usuario GetLoggedUser();
        public void SetHttpContext(HttpContext _httpContext);
    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppBibliotecaContext app;
        private HttpContext httpContext;
        public UsuarioRepository(AppBibliotecaContext _app)
        {
            this.app = _app;
        }
        public Usuario FindUserLogin(string username, string password)
        {
            return app.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();
        }

        public Usuario GetLoggedUser()
        {
            var claim = httpContext.User.Claims.FirstOrDefault();
            var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
        public void SetHttpContext(HttpContext _httpContext)
        {
            this.httpContext = _httpContext;
        }
    }
}
