﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Respositories
{
    public interface ILibroRepository
    {
        public List<Libro> Libros();
        public Libro LibroDetalles(int id);
        public void AddComentario(Comentario comentario, Usuario user);

    }
    public class LibroRepository : ILibroRepository
    {
        private readonly AppBibliotecaContext app;
        public LibroRepository(AppBibliotecaContext _app)
        {
            this.app = _app;
        }

        public List<Libro> Libros()
        {
            return app.Libros.Include(o => o.Autor).ToList();
        }
        public Libro LibroDetalles(int id)
        {
            return app.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .Where(o => o.Id == id)
                .FirstOrDefault();
        }

        public void AddComentario(Comentario comentario, Usuario user)
        {
            comentario.UsuarioId = user.Id;
            comentario.Fecha = DateTime.Now;
            app.Comentarios.Add(comentario);

            var libro = app.Libros.Where(o => o.Id == comentario.LibroId).FirstOrDefault();
            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            app.SaveChanges();
        }
    }
}
