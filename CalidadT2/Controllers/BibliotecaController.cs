﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Respositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly IUsuarioRepository repositoryU;
        private readonly IBibiotecaRepository repositoryB;
        public BibliotecaController(IUsuarioRepository _repositoryU, IBibiotecaRepository _repositoryB)
        {
            this.repositoryU = _repositoryU;
            this.repositoryB = _repositoryB;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Usuario user = LoggedUser();

            var model = repositoryB.VerBiblioteca(user);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Usuario user = LoggedUser();
            repositoryB.SetTempData(TempData);
            repositoryB.AddLibro(libro, user);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Usuario user = LoggedUser();
            repositoryB.SetTempData(TempData);
            repositoryB.MarcarLeyendo(libroId, user);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user = LoggedUser();
            repositoryB.SetTempData(TempData);
            repositoryB.MarcarTerminado(libroId, user);
            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {
            repositoryU.SetHttpContext(HttpContext);
            var user = repositoryU.GetLoggedUser();
            return user;
        }
    }
}
