﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Respositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ILibroRepository repositoryL;
        private readonly IUsuarioRepository repositoryU;

        public LibroController(ILibroRepository _repository, IUsuarioRepository _repositoryU)
        {
            this.repositoryL = _repository;
            this.repositoryU = _repositoryU;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model= repositoryL.LibroDetalles(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Usuario user = LoggedUser();
            repositoryL.AddComentario(comentario,user);

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        private Usuario LoggedUser()
        {
            repositoryU.SetHttpContext(HttpContext);
            var user = repositoryU.GetLoggedUser();
            return user;
        }
    }
}
